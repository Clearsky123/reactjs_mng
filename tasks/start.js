'use strict';
const path = require('path');

module.exports = function (projDir) {
  const paths = require('./paths')(projDir);
  require(path.join(paths.dist, 'server.js'));
}

if (!module.parent) {
  module.exports(path.join(__dirname, '..'));
}
