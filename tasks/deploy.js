'use strict';
const assert = require('assert');
const fs = require('fs');
const Git = require('nodegit');
const path = require('path');
const Rsync = require('rsync');
const sh = require('shelljs');
const expandTilde = require('expand-tilde');

sh.set('-e');

module.exports = function deploy(projDir, name) {
  const paths = require('./paths')(projDir);
  const config = require(path.join(paths.proj, 'config'));
  name = name || 'default';
  const deploy = config && config.deploy && config.deploy[name];
  assert(deploy || deploy, `Deploy "${name}" is not configured!`);
  assert(deploy.url, `URL of deploy "${name}" is not configured!`);
  const deployPath = path.join(paths.deploy, name);
  const deployRemote = deploy.remote || 'origin';
  const deployBranch = deploy.branch || 'master';

  sh.cd(paths.proj);
  const env = process.env;
  env.NODE_ENV = 'production';
  sh.exec('npm run build', {env});

  sh.mkdir('-p', deployPath);

  var credentialsCallback = null;
  if (deploy.sshAgent) {
    credentialsCallback = function(url, userName) {
      return Git.Cred.sshKeyFromAgent(userName || deploy.userName);
    }
  } else if (deploy.publicKey || deploy.privateKey || deploy.passphrase) {
    assert(deploy.publicKey && deploy.privateKey, 'Public and private keys are required!');
    const publicKey = path.resolve(paths.proj, expandTilde(deploy.publicKey));
    const privateKey = path.resolve(paths.proj, expandTilde(deploy.privateKey));
    credentialsCallback = function(url, userName) {
      return Git.Cred.sshKeyNew(userName || deploy.userName, publicKey, privateKey, deploy.passphrase || '');
    }
  } else {
    credentialsCallback = function(url, userName) {
      userName = userName || deploy.userName;
      return userName ? Git.Cred.userpassPlaintextNew(userName, deploy.password || '') : Git.Cred.defaultNew();
    }
  }

  const fetchOpts = {callbacks: {credentials: credentialsCallback}};

  const repoPromise = Git.Clone(deploy.url, deployPath, {fetchOpts: fetchOpts}).catch(err => {
    console.error(err && (err.message || err));
    console.log('Trying to open the repository instead of cloning ...');
    return Git.Repository.open(deployPath)
  });

  repoPromise.then(repo =>
    repo.getRemote(deployRemote)
  ).then(remote => {
    var originalRemoteUrl = remote.url();
    const remoteUrl = path.join(path.dirname(originalRemoteUrl), path.basename(originalRemoteUrl, '.git'));
    const deployUrl = path.join(path.dirname(deploy.url), path.basename(deploy.url, '.git'));
    assert(remoteUrl === deployUrl, `The local repository's URL "${originalRemoteUrl}" does not match the configured URL "${deploy.url}"!`);
    return repoPromise;
  }).then(repo =>
    repo.fetch(deployRemote, fetchOpts).then(() => repo)
  ).then(repo =>
    repo.getBranch(deployBranch)
  ).then(branch =>{
    if (Git.Branch.delete(branch) === 0) {
      return repoPromise.then(repo => repo.checkoutBranch(deployBranch).then(() => repo));
    }
    return repoPromise.then(repo =>
        repo.mergeBranches(branch, `${deployRemote}/${deployBranch}`,
          Git.Signature.default(repo),
          Git.Merge.PREFERENCE.FASTFORWARD_ONLY)
    );
  }).then(() => {
    const rsync = new Rsync()
      .flags('rlptumq')
      .set('delete')
      .set('force')
      .exclude(['.*', '/start.js'])
      .output(data => {
        console.log(data.toString('utf8'));
      }, data => {
        console.error(data.toString('utf8'));
      })
      .source(paths.dist + '/')
      .destination(deployPath);
    console.log(rsync.command());
    return new Promise(resolve => {
      rsync.execute(err => {
        if (err) {
          throw err;
        }
        resolve();
      });
    });
  }).done(() => console.log(`The content of "${paths.dist}" has been deployed to "${deployPath}".`));
};

if (!module.parent) {
  module.exports(path.join(__dirname, '..'), process.argv[2]);
}
