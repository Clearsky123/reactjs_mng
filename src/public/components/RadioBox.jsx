'use strict';
import {Form} from 'semantic-ui-react';
import React from 'react';

import {generateId} from '../util';

export default class extends React.Component {
  componentWillMount() {
    this._id = this.props.id || generateId(this.props.label || this.props.name);
  }

  render() {
    const {id = this._id, label, ...props} = this.props;
    return (
      <Form.Field>
        <div className="ui radio checkbox">
          <input tabIndex="0" {...props} className="hidden" id={id} type="radio"/>
          <label htmlFor={id}>{label}</label>
        </div>
      </Form.Field>
    );
  }
}
