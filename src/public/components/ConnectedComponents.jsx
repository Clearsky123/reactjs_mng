'use strict';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as Components from './Components';
import PlainFormInput from './FormInput';
import PlainSection from './Section';
import {toggleSection, changeInput} from '../actions';

export const Section = connect(
  (state, {name}) => ({active: state.getIn(['ui', 'sections', name])}),
  (dispatch, {name}) => bindActionCreators({
    onTitleClick: toggleSection.bind(null, name),
  }, dispatch)
)(PlainSection);

export const FormInput = connect(
  (state, {name}) => ({value: state.get(name)}),
  (dispatch, {name}) => bindActionCreators({
    onChange: changeInput.bind(null, name),
  }, dispatch)
)(PlainFormInput);
