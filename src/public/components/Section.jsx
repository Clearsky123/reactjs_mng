'use strict';
import {Accordion, Header, Icon} from 'semantic-ui-react';
import React from 'react';

export default function({title, error, children, defaultActive, active, onTitleClick}) {
  const activeIndex = {};
  if (active !== undefined) {
    activeIndex.activeIndex = active ? 0 : -1;
  }
  if (defaultActive !== undefined) {
    activeIndex.defaultActiveIndex = defaultActive ? 0 : -1;
  }
  return (
    <Accordion styled fluid style={{background: 'transparent'}} {...activeIndex} onTitleClick={onTitleClick}>
      <Accordion.Title>
        <Header dividing style={{marginBottom: 0, color: 'inherit'}}>
          {title}
          {error ? <Icon name="warning sign" color="yellow" style={{display: 'inline', marginLeft: '0.5em', fontSize: '1em'}}/> : null}
          <Icon name="dropdown"/>
        </Header>
      </Accordion.Title>
      <Accordion.Content>
        {children}
      </Accordion.Content>
    </Accordion>
  );
}
