'use strict';
import {connect} from 'react-redux';
import {Icon} from 'semantic-ui-react';
import {createSelector} from 'reselect';
import React from 'react';

import {togglePhysicalTopology} from '../actions';
import {nodeNames, stateSelector} from '../physical-topology-util';
import * as ui from '../ui/variables';

const commonStyle = {
  textAlign: 'center',
  width: '2em',
  height: '2em',
  borderWidth: 1,
  borderStyle: 'solid',
  borderColor: 'transparent',
};
const style = {
  ...commonStyle,
  borderColor: ui.solidBorderColor,
};
const topStyle = {
  ...commonStyle,
  borderBottomColor: ui.solidBorderColor,
};
const leftStyle = {
  ...commonStyle,
  borderRightColor: ui.solidBorderColor,
  minWidth: '1em',
};
const rightStyle = {
  ...commonStyle,
  borderLeftColor: ui.solidBorderColor,
  minWidth: '1em',
};
const currentStyle = {
  fontWeight: 'bold',
  color: ui.primaryColor,
};

class PhysTopoMatEditor extends React.Component {
  constructor() {
    super();
    this.state = {currentRow: -1, currentCol: -1};
    this._handleMouseEnter = this._handleMouseEnter.bind(this);
    this._handleMouseLeave = this._handleMouseLeave.bind(this);
  }

  _handleMouseEnter(event) {
    const cell = event.currentTarget;
    this.setState({
      currentRow: parseInt(cell.dataset.row, 10),
      currentCol: parseInt(cell.dataset.col, 10),
    });
  }

  _handleMouseLeave() {
    this.setState({currentRow: -1, currentCol: -1});
  }

  render() {
    const {topo, onToggle, ...props} = this.props;
    const nodes = nodeNames(topo.order);
    const {currentRow, currentCol} = this.state;
    return (
      <div {...props}>
        <table style={{borderCollapse: 'collapse', borderSpacing: 0, border: 'none'}}>
          <tbody>
            <tr>
              <td style={commonStyle}/>
              {nodes.map((col, colIndex) => (
                <td key={col} style={colIndex === currentCol ? {...topStyle, ...currentStyle} : topStyle}>{col}</td>
              ))}
              <td style={rightStyle}/>
            </tr>
            {
              nodes.map((row, rowIndex) => (
                <tr key={row}>
                  <td style={rowIndex === currentRow ? {...leftStyle, ...currentStyle} : leftStyle}>{row}</td>
                  {
                    nodes.map((col, colIndex) => {
                      if (row === col) {
                        if (row === 'S' || row === 'L') {
                          return (
                            <td key={col} data-row={rowIndex} data-col={colIndex}
                                onMouseEnter={this._handleMouseEnter} onMouseLeave={this._handleMouseLeave}
                                style={{...style, backgroundColor: ui.disabledBackgroundColor}}/>
                          );
                        }
                        return (
                          <td key={col} data-row={rowIndex} data-col={colIndex}
                              onMouseEnter={this._handleMouseEnter} onMouseLeave={this._handleMouseLeave}
                              style={{...style, color: ui.unselectedTextColor}}>
                            <Icon name="check circle"/>
                          </td>
                        );
                      }
                      return (
                        <td key={col} data-row={rowIndex} data-col={colIndex} style={style}
                            onMouseEnter={this._handleMouseEnter} onMouseLeave={this._handleMouseLeave}
                            onClick={onToggle && onToggle.bind(this, row, col)}>
                            {topo[row][col] ? <Icon name="check circle"/> : null}
                        </td>
                      );
                    })
                  }
                  <td style={rightStyle}/>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    );
  }
}

export default connect(
  stateSelector,
  {onToggle: togglePhysicalTopology}
)(PhysTopoMatEditor);
