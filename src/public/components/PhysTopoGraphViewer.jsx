'use strict';
import {connect} from 'react-redux';
import {createSelector} from 'reselect';
import React from 'react';
import * as d3 from 'd3-selection';
import {extent} from 'd3-array';
import 'd3-transition';

import * as ui from '../ui/variables';
import {nodeNames, stateSelector} from '../physical-topology-util';

const RADIUS = 20;
const STEP = 5 * RADIUS;
const FONT_SIZE = RADIUS * 0.8;
const EDGE_WIDTH = RADIUS * 0.2;
const EDGE_DASH = `${RADIUS / 4}, ${RADIUS / 2}`;
const TRANSITION_DURATION_MS = 250;

function getNodes(topo) {
  return nodeNames(topo.order).map((name, index) => ({name, x: index * STEP, y: 0}));
}

function getEdges(topo, nodes) {
  const n = topo.order + 1;
  const edges = [];
  var yStep = STEP;
  for (let i = 0; i < n; ++i) {
    const row = i === 0 ? 'S' : i;
    for (let j = n; j > i; --j) {
      const col = j === n ? 'L' : j;
      if (topo[row][col]) {
        edges.push({source: i, target: j, primary: j === i+1});
        if (nodes[i].y === nodes[j].y && j !== i+1) {
          if (j === i+2) {
            for (let k = j; k <= n; ++k) {
              nodes[k].x -= STEP;
            }
            nodes[j-1].x -= STEP / 2;
            nodes[j-1].y += yStep;
          } else {
            const twoSteps = STEP * 2;
            for (let k = j; k <= n; ++k) {
              nodes[k].x -= twoSteps;
            }
            if (i > 0 && nodes[i-1].x === nodes[i].x && nodes[i-1].y === nodes[i].y + yStep) {
              yStep = -yStep;
            }
            for (let k = i+1; k < j; ++k) {
              nodes[k].x -= STEP;
              nodes[k].y += yStep;
            }
          }
        }
      }
    }
  }
  return edges;
}

class PhysTopoGraphViewer extends React.Component {
  componentDidMount() {
    this._render();
  }

  componentDidUpdate() {
    this._render();
  }

  _render() {
    if (!this._svg) {
      return;
    }
    const svg = d3.select(this._svg);
    const topo = this.props.topo;

    const nodes = getNodes(topo);
    const edges = getEdges(topo, nodes);
    const xExtent = extent(nodes, n => n.x);
    const yExtent = extent(nodes, n => n.y);
    const viewBoxMinX = xExtent[0] - RADIUS;
    const viewBoxMinY = yExtent[0] - RADIUS;
    const viewBoxWidth = xExtent[1] + RADIUS - viewBoxMinX;
    const viewBoxHeight = yExtent[1] + RADIUS - viewBoxMinY;
    const width = viewBoxWidth / RADIUS;
    svg.style('min-width', `${width * 10}px`).style('max-width', `${width * 20}px`)
      .transition().duration(TRANSITION_DURATION_MS).attr('viewBox', `${viewBoxMinX} ${viewBoxMinY} ${viewBoxWidth} ${viewBoxHeight}`);

    const edgeLines = svg.select('g.edges').selectAll('line')
      .data(edges, e => e.source + ',' + e.target);
    edgeLines.transition()
      .attr('x1', e => nodes[e.source].x)
      .attr('y1', e => nodes[e.source].y)
      .attr('x2', e => nodes[e.target].x)
      .attr('y2', e => nodes[e.target].y);
    edgeLines.enter().append('line').attr('stroke', ui.mutedTextColor).attr('stroke-width', EDGE_WIDTH)
      .attr('stroke-dasharray', e => e.primary ? 'none' : EDGE_DASH)
      .attr('stroke-linecap', 'round')
      .attr('x1', e => nodes[e.source].x)
      .attr('y1', e => nodes[e.source].y)
      .attr('x2', e => nodes[e.target].x)
      .attr('y2', e => nodes[e.target].y);
    edgeLines.exit().remove();

    const nodeCircles = svg.select('g.nodes').selectAll('g.node').data(nodes, n => n.name);
    nodeCircles.transition().attr('transform', n => `translate(${n.x}, ${n.y})`);
    const newNodeCircles = nodeCircles.enter().append('g')
      .attr('class', 'node')
      .attr('transform', n => `translate(${n.x}, ${n.y})`);
    newNodeCircles.append('circle').attr('cx', 0).attr('cy', 0)
      .attr('r', RADIUS).attr('stroke', 'none')
      .attr('fill', n => n.name === 'S' || n.name === 'L' ? ui.black : ui.primaryColor);
    newNodeCircles.append('text').text(n => n.name)
      .attr('text-anchor', 'middle')
      .attr('dominant-baseline', 'middle')
      .attr('fill', ui.offWhite)
      .attr('font-size', FONT_SIZE)
      .attr('font-weight', 'bold');
    nodeCircles.exit().remove();
  }

  render() {
    const {topo, ...props} = this.props;
    return (
      <div {...props}>
        <svg preserveAspectRatio="xMidYMid meet" ref={ref => this._svg = ref} style={{transitionProperty: 'min-width, max-width', transitionDuration: TRANSITION_DURATION_MS + 'ms'}}>
          <g className="edges"></g>
          <g className="nodes"></g>
        </svg>
      </div>
    );
  }
}

export default connect(stateSelector, () => ({}))(PhysTopoGraphViewer);
