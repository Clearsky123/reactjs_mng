'use strict';
import {Form, Grid, Icon} from 'semantic-ui-react';
import {connect} from 'react-redux';
import React from 'react';
import {createSelector} from 'reselect';
import classNames from 'classnames';

import {Section} from './ConnectedComponents';
import {changeTransZeros, changeTransZeroValue} from '../actions';
import {generateId, nextTask} from '../util';

function Button({disabled, children, ...props}) {
  var className = 'ui basic icon borderless button';
  if (disabled) {
    className = classNames(className, 'disabled');
    props.tabIndex = -1;
  }
  return (
    <button className={className} style={{padding: 0}} type='button' {...props}>
      {children}
    </button>
  );
}

class TransZeroInput extends React.Component {
  constructor() {
    super();
    this.state = {buttons: false};
    this._handleMouseEnter = this._handleMouseEnter.bind(this);
    this._handleMouseLeave = this._handleMouseLeave.bind(this);
    this._realInput = null;
    this._imageInput = null;
  }

  focus() {
    if (this._imageInput) {
      this._imageInput.focus();
    }
  }

  _handleMouseEnter() {
    this.setState({buttons: true});
  }

  _handleMouseLeave() {
    this.setState({buttons: false});
  }

  render() {
    const {
      label, real, image, active,
      onRealChange, onImageChange,
      onRemove,
      onMoveToPrev, onMoveToNext,
      onAddBefore, onAddAfter,
      onFocus, onBlur,
    } = this.props;
    const {buttons} = this.state;
    const myButtons = buttons || active ? (
      <div style={{display: 'inline-block', float: 'right'}}>
        <Button title="Add Before" onClick={onAddBefore}>
          <Icon.Group>
            <Icon name="plus"/>
            <Icon corner name="arrow left"/>
          </Icon.Group>
        </Button>
        <Button title="Add After" onClick={onAddAfter}>
          <Icon.Group>
            <Icon name="plus"/>
            <Icon corner name="arrow right"/>
          </Icon.Group>
        </Button>
        <Button disabled={!onMoveToPrev} title="Move to Previous" onClick={onMoveToPrev}>
          <Icon name="arrow left"/>
        </Button>
        <Button disabled={!onMoveToNext} title="Move to Next" onClick={onMoveToNext}>
          <Icon name="arrow right"/>
        </Button>
        <Button title="Remove" onClick={onRemove}>
          <Icon name="remove"/>
        </Button>
      </div>
    ) : null;
    return (
      <Grid.Column width={12} mobile={12} tablet={6} computer={4} largeScreen={3} widescreen={2}>
        <Form.Field onFocus={onFocus} onBlur={onBlur} onMouseEnter={this._handleMouseEnter} onMouseLeave={this._handleMouseLeave}>
          <div className="label">
            {label}
            {myButtons}
          </div>
          <Form.Group widths={2} style={{flexWrap: 'nowrap'}}>
            <Form.Field className={classNames({active})}>
            <input placeholder="Real" type="number" value={real} onChange={onRealChange} ref={ref => this._realInput = ref}/>
            </Form.Field>
            <Form.Field className={classNames({active})}>
              <input placeholder="Image" type="number" value={image} onChange={onImageChange} ref={ref => this._imageInput = ref}/>
            </Form.Field>
          </Form.Group>
        </Form.Field>
      </Grid.Column>
    );
  }
}

class TransZerosAccordion extends React.Component {
  constructor() {
    super();
    this.state = {activeIndex: -1};
    this._cancelBlur = null;
  }

  _handleFocus(index) {
    if (this._cancelBlur) {
      this._cancelBlur();
      this._cancelBlur = null;
    }
    if (this.state.activeIndex !== index) {
      this.setState({activeIndex: index});
    }
  }

  _handleBlur(index) {
    if (this.state.activeIndex !== -1) {
      this._cancelBlur = nextTask(() => this.setState({activeIndex: -1}));
    }
  }

  _handleRemove(onRemove, index) {
    if (index === this.state.activeIndex) {
      this.setState({activeIndex: -1});
    }
    onRemove.call(this, index);
  }

  _handleMoveToPrev(onMoveToPrev, index) {
    if (index === this.state.activeIndex) {
      this.setState({activeIndex: index - 1});
    }
    onMoveToPrev.call(this, index);
  }

  _handleMoveToNext(onMoveToNext, index) {
    if (index === this.state.activeIndex) {
      this.setState({activeIndex: index + 1});
    }
    onMoveToNext.call(this, index);
  }

  _handleInsert(onInsert, index) {
    if (index === this.state.activeIndex) {
      this.setState({activeIndex: index+1});
    }
    onInsert.call(this, index);
  }

  render() {
    const {
      transZeros=[],
      onAdd, onAddAfter, onMoveToPrev, onMoveToNext,
      onRealChange, onImageChange, onRemove,
      ...props
    } = this.props;
    const {activeIndex} = this.state;
    const lastIndex = transZeros.length - 1;
    const children = transZeros.map(({id, real, image}, i) => (
      <TransZeroInput key={id} label={(i+1) + '.'} real={real} image={image}
                      active={activeIndex === i}
                      onRealChange={onRealChange && onRealChange.bind(this, i)}
                      onImageChange={onImageChange && onImageChange.bind(this, i)}
                      onRemove={onRemove && this._handleRemove.bind(this, onRemove, i)}
                      onAddBefore={onAdd && this._handleInsert.bind(this, onAdd, i)}
                      onAddAfter={onAddAfter && onAddAfter.bind(this, i)}
                      onMoveToPrev={i > 0 && onMoveToPrev ? this._handleMoveToPrev.bind(this, onMoveToPrev, i) : undefined}
                      onMoveToNext={i < lastIndex && onMoveToNext ? this._handleMoveToNext.bind(this, onMoveToNext, i) : undefined}
                      onFocus={this._handleFocus.bind(this, i)}
                      onBlur={this._handleBlur.bind(this, i)}/>
    ));
    if (onAdd) {
      children.push(
        <Grid.Column key={-1} width={2}>
          <Form.Field>
            <div className="label">
              <Button title="Add" onClick={onAdd && onAdd.bind(this, transZeros.length)}>
                <Icon name="plus"/>
              </Button>
            </div>
          </Form.Field>
        </Grid.Column>
      );
    }
    return (
      <Section title="Transmission Zeros" name="cplMatSyn_transZeros" {...props}>
        <Form>
          <Grid>
            {children}
          </Grid>
        </Form>
      </Section>
    );
  }
}

export default connect(
  createSelector(state => state.get('transZeros'), tz => ({transZeros: tz.toJS()})),
  {
    onAdd: changeTransZeros.bind(null, 'insert'),
    onRealChange: changeTransZeroValue.bind(null, 'real'),
    onImageChange: changeTransZeroValue.bind(null, 'image'),
    onRemove: changeTransZeros.bind(null, 'remove'),
    onAddAfter: changeTransZeros.bind(null, 'insertAfter'),
    onMoveToPrev: changeTransZeros.bind(null, 'moveToPrev'),
    onMoveToNext: changeTransZeros.bind(null, 'moveToNext'),
  }
)(TransZerosAccordion);
