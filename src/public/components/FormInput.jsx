'use strict';
import {Form, Label} from 'semantic-ui-react';
import React from 'react';

import {generateId} from '../util';

export default class extends React.Component {
  constructor() {
    super();
    this._oldValid = true;
    this._valid = true;
  }

  componentWillMount() {
    this._id = this.props.id || generateId(this.props.label || this.props.name);
  }

  _triggerValidityChangeCallback() {
    if (this.props.onValidityChange && this._oldValid !== this._valid) {
      this.props.onValidityChange(this._valid);
    }
  }

  componentDidMount() {
    this._triggerValidityChangeCallback();
  }

  componentDidUpdate() {
    this._triggerValidityChangeCallback();
  }

  _selectOnFocus(onFocus, event) {
    event.target.select();
    if (onFocus) {
      onFocus = onFocus(event);
    }
  }

  _handleKeyPress(charPattern, event) {
    const key = event.key;
    if (key.length === 1 && !charPattern.test(event.key)) {
      event.preventDefault();
    }
  }

  render() {
    const {
      id = this._id,
      inline,
      label,
      placeholder,
      suffix,
      value,
      error,
      validate,
      onValidityChange,
      autoSelect,
      onFocus,
      charPattern,
      ...props,
    } = this.props;
    const myPlaceholder = placeholder === true ? label : placeholder;
    var myError = error;
    if (validate && value != undefined) {
      this._oldValid = this._valid;
      this._valid = validate(value);
      myError = !this._valid;
    }
    var myOnFocus = onFocus;
    if (autoSelect) {
      myOnFocus = this._selectOnFocus.bind(this, myOnFocus);
    }
    const onKeyPress = charPattern ? this._handleKeyPress.bind(this, charPattern) : null;
    const control = suffix ? (
      <div className="ui right labeled input">
        <input id={id} placeholder={myPlaceholder} value={value} onFocus={myOnFocus}
               onKeyPress={onKeyPress} {...props}/>
        <Label as="label" basic htmlFor={id}>{suffix}</Label>
      </div>
    ) : (
      <input id={id} placeholder={myPlaceholder} value={value} onFocus={myOnFocus}
             onKeyPress={onKeyPress} {...props}/>
    );
    return (
      <Form.Field error={myError} inline={inline}>
        <label htmlFor={id}>{label}</label>
        {control}
      </Form.Field>
    );
  }
}
