'use strict';
import {Map, fromJS} from 'immutable';

import * as Actions from './actions';
import {defaultPhysicalTopology} from './default-state';
import {generateId} from './util';

function changeTransZeros(tzs, name, index) {
  switch (name) {
    case 'insert':
      return tzs.insert(index, Map({id: generateId(), real: '', image: ''}));
    case 'remove':
      return tzs.remove(index);
    case 'insertAfter':
      return tzs.insert(index+1, Map({id: generateId(), real: '', image: ''}));
    case 'moveToPrev':
      if (index > 0) {
        return tzs.remove(index).insert(index-1, tzs.get(index));
      }
      break;
    case 'moveToNext':
      if (index < tzs.size - 1) {
        return tzs.remove(index).insert(index+1, tzs.get(index));
      }
      break;
  }
  return tzs;
}

function togglePhysicalTopology(topo, row, col) {
  if (row === col) {
    return topo;
  }
  const valuePath = [row, col];
  if (topo.getIn(valuePath, false)) {
    topo = topo.removeIn(valuePath);
    return topo.get(row).size ? topo : topo.remove(row);
  }
  return topo.setIn(valuePath, true);
}

export default function(state, {type, ...action}) {
  switch (type) {
    case Actions.CHANGE_TRANS_ZEROS:
      return state.update('transZeros', tzs => changeTransZeros(tzs, action.name, action.index));
    case Actions.CHANGE_TRANS_ZERO_VALUE:
      return state.update('transZeros', tzs => tzs.set(action.index, tzs.get(action.index).set(action.name, action.value)));
    case Actions.CHANGE_INPUT:
      return state.set(action.name, action.value);
    case Actions.TOGGLE_SECTION:
      return state.updateIn(['ui', 'sections', action.name], v => !v);
    case Actions.TOGGLE_PHYSICAL_TOPOLOGY:
      return state.update('physicalTopology', topo => togglePhysicalTopology(topo, action.row, action.col));
    case Actions.CHANGE_FILTER_ORDER:
      state = state.set('filterOrder', action.value);
      if (action.value) {
        return state.update('physicalTopology', topo => {
          const newOrder = parseInt(action.value, 10);
          if (newOrder !== topo.get('order')) {
            topo = fromJS(defaultPhysicalTopology(newOrder));
          }
          return topo;
        });
      }
      return state;
  }
  return state;
}
