'use strict';

export const TOGGLE_SECTION = 'TOGGLE_SECTION';
export const CHANGE_INPUT = 'CHANGE_INPUT';
export const CHANGE_TRANS_ZEROS = 'CHANGE_TRANS_ZEROS';
export const CHANGE_TRANS_ZERO_VALUE = 'CHANGE_TRANS_ZERO_VALUE';
export const TOGGLE_PHYSICAL_TOPOLOGY = 'TOGGLE_PHYSICAL_TOPOLOGY';
export const CHANGE_FILTER_ORDER = 'CHANGE_FILTER_ORDER';

export function toggleSection(name) {
  return {type: TOGGLE_SECTION, name};
}

export function changeInput(name, event) {
  return {type: CHANGE_INPUT, name, value: event.target.value.trim()};
}

export function changeTransZeros(name, index) {
  return {type: CHANGE_TRANS_ZEROS, name, index};
}

export function changeTransZeroValue(name, index, event) {
  return {type: CHANGE_TRANS_ZERO_VALUE, name, index, value: event.target.value};
}

export function togglePhysicalTopology(row, col) {
  if (row !== 'S' && col !== 'L'
    && (col === 'S' || row === 'L' || parseInt(row, 10) > parseInt(col, 10))) {
    const tmp = row;
    row = col;
    col = tmp;
  }
  return {type: TOGGLE_PHYSICAL_TOPOLOGY, row, col};
}

export function changeFilterOrder(value) {
  return {type: CHANGE_FILTER_ORDER, value};
}
