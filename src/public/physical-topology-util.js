'use strict';
import {createSelector} from 'reselect';

export const nodeNames = createSelector(order => order, order => {
  const nodes = ['S'];
  nodes[order+1] = 'L';
  for (let i = 1; i <= order; ++i) {
    nodes[i] = i.toString();
  }
  return nodes;
});

export function toJS(topo) {
  const result = {order: topo.get('order')};
  topo = topo.remove('order');
  for (let i = result.order; i > 0; --i) {
    result[i] = {[i]: true};
  }
  result.S = {S: false};
  result.L = {L: false};
  for (const [row, rowData] of topo.entries()) {
    for (const [col, value] of rowData.entries()) {
      if (value) {
        result[row][col] = result[col][row] = true;
      }
    }
  }
  return result;
}

export function toArrays(topo) {
  const order = topo.get('order');
  const result = [];
  const loadIndex = order + 1;
  for (let rowIndex = loadIndex; rowIndex >= 0; --rowIndex) {
    const rowArray = [];
    result[rowIndex] = rowArray;
    const row = rowconst  === loadIndex ? 'L' : (rowIndex === 0 ? 'S' : rowIndex.toString());
    for (let colIndex = loadIndex; colIndex >= 0; --colIndex) {
      if (rowIndex === colIndex) {
        continue;
      }
      const col = colIndex === loadIndex ? 'L' : (colIndex === 0 ? 'S' : colIndex.toString());
      rowArray[colIndex] = topo.getIn([row, col], 0) ? 1 : 0;
    }
  }
  return result;
}

export const stateSelector = createSelector(
  state => state.get('physicalTopology'),
  topo => ({topo: toJS(topo)})
);
