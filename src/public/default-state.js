'use strict';
import {fromJS} from 'immutable';

export function defaultPhysicalTopology(order) {
  const result = {order};
  result.S = {'1': true};
  result[order] = {L: true};
  for (let i = order - 1; i > 0; --i) {
    result[i] = {[i+1]: true};
  }
  return result;
}

export default fromJS({
  filterOrder: '1',
  startFreq: '',
  stopFreq: '',
  returnLoss: '',
  unloadedQ: '',
  filterMode: 'TE',
  transZeros: [],
  physicalTopology: defaultPhysicalTopology(1),
  ui: {
    sections: {
      cplMatSyn_physicalTopology: true,
      cplMatSyn_parameters: true,
      cplMatSyn_transZeros: true,
    },
  },
});
